import {Validators, FormBuilder, FormGroup } from '@angular/forms';
export class FormEditUser {



     form : FormGroup;
     formBuilder: FormBuilder
     user:any;

 constructor(user:any){
     this.formBuilder = new FormBuilder();
  }


  /**
   * create the validation and structure of the edit user form
   */
  GetFormBuilder(): FormGroup{

   this.form = this.formBuilder.group({
        Theme: ['',[Validators.required]],
        Certificates:['',[Validators.required]],
        Tasks:['',[Validators.required]],
        Courseware:['',[Validators.required]],
        Course:['',[Validators.required]],
        Search:['',[Validators.required]],
        Lenguage:['',[Validators.required]],
        TimeZone:['',[Validators.required]],
        Edxnote:['',[Validators.required]],
        Email: ['',[Validators.required,Validators.pattern('^[a-z0-9A-Z_]+(\.[_a-z0-9A-Z]+)*@[a-z0-9-A-Z]+(\.[a-z0-9-A-Z]+)*(\.[a-zA-Z]{2,15})$')]],
     });

    return this.form;
   }







}
