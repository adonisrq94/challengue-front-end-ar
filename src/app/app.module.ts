import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {app_routing} from './app.routes';

import { AppComponent } from './app.component';

// Services
import { UserService } from './services/user.service';

// Component
import { NavbarComponent } from './components/sharedComponents/navbar/navbar.component';
import { SelectComponent } from './components/select/select.component';
import { UserComponent } from './components/user/user.component';

//classes
import {User} from './classes/user';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SelectComponent,
    UserComponent,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    app_routing,
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [
    UserService,
    User

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
