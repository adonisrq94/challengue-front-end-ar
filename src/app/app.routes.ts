import { RouterModule, Routes } from '@angular/router';


import { SelectComponent } from './components/select/select.component';
import { UserComponent } from './components/user/user.component';

/**
 * routes of the app 
 */
const app_routes: Routes = [
  { path: 'select', component: SelectComponent },
  { path: 'home/:id', component: UserComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'select' }
];

export const app_routing = RouterModule.forRoot(app_routes);
