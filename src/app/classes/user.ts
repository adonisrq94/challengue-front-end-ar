import { UserService } from '../services/user.service';
import { Features } from '../interfaces/features.interface';


export class  User {

    private id: string = '';

    /**
     * banner of user
     */
    private banner_message: string = '';
    /**
     * last pay of the user
     */
    private last_payment_date: string = '';
    /**
     * the opcion theme
     */
    private theme_name: string = '';

    /**
     *  the image of user
     */
    private user_profile_image: string;
    /**
     * timezone
     */
    private displayed_timezone: string = '';
    /**
     * lenguage
     */
    private language_code: string = '';
    /**
     * date of firts activity
     */
    private creation_date: string = '';
    /**
     * Lmail
     */
    private user_email: string = '';
    /**
     * susbcription the users has
     */
    private susbcription: string = '';
    /**
     * List of Features
     */
    private enabled_features:Features;

    /**
     * informacion of user in json format
     */
    public userJson:any=[];


    constructor() {

    }

    /*
      Getter and setter
    */

    public getId():string{
      return this.id;
    }

    public setId(id:string){
      this.id=id;
    }

    public getBanner():string{
      return this.banner_message;
    }

    public setBanner(banner_message:string){
      this.banner_message=banner_message;
    }

    public getDatePayment():string{
      return this.last_payment_date;
    }

    public setDatePayment(last_payment_date:string){
      this.last_payment_date=last_payment_date;
    }

    public getTheme():string{
      return this.theme_name;
    }

    public setTheme(theme_name:string){
      this.theme_name=theme_name;
    }

    public getImage():string{
      return this.user_profile_image;
    }

    public setImage(user_profile_image:string){
      this.user_profile_image=user_profile_image;
    }

    public getTimeZone():string{
      return this.displayed_timezone;
    }

    public setTimeZone(displayed_timezone:string){
      this.displayed_timezone=displayed_timezone;
    }

    public getLenguaje():string{
      return this.language_code;
    }

    public setLenguaje(language_code:string){
      this.language_code=language_code;
    }

    public getDateCreate():string{
      return this.creation_date;
    }

    public setDateCreate(creation_date:string){
      this.creation_date=creation_date;
    }

    public getEmail():string{
      return this.user_email;
    }

    public setEmail(user_email:string){
      this.user_email=user_email;
    }

    public getSubscription():string{
      return this.susbcription ;
    }

    public setSubscription(susbcription:string){
      this.susbcription=susbcription;
    }



}
