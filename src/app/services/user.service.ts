import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers,Response,RequestMethod} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Data} from '../interfaces/data.interface';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class UserService {
  users:any [] = [];
  user:any  = {};
  userJson:any = {};

  /**
   * url of api rest 
   */
  private postsUrl:string = "http://localhost:8010/api/v1";

  constructor(private http:Http) { }


  /**
   * make the request for all information users
   */
  public getUsers() {
      return this.http.get( `${this.postsUrl}/customerdata/` ).map( response => {
          this.users = response.json();
          if (this.users && this.users['count']>0){
             this.userJson = {"status":"200","data":this.users['results']};
          }else{
            this.userJson = [{"status":"404","data":{}}];
          }
          return this.userJson;

      }).catch((error: any) => Observable.throw(error.json()));
  }

  /**
   * make the request for specific information  user
   */
  public getUser(id:string) {
      return this.http.get( `${this.postsUrl}/customerdata/${id}` ).map( response => {
          this.user = response.json();
          return response.json();

      }).catch((error: any) => Observable.throw(error.json()));
  }

  /**
   * make the request for updateUser the information of the user
   */
  public updateUser(paramsEdit:any,params:any) {
      let body: Data =
        {
            id: params.id,
                //structure of  Object user
                data: {
                    banner_message: params.data.banner_message,
                    LAST_PAYMENT_DATE: params.data.LAST_PAYMENT_DATE,
                    theme_name: paramsEdit.Theme,
                    user_profile_image: params.data.user_profile_image,
                    ENABLED_FEATURES: {
                        CERTIFICATES_INSTRUCTOR_GENERATION: paramsEdit.Certificates,
                        ENABLE_COURSEWARE_SEARCH: paramsEdit.Courseware,
                        ENABLE_EDXNOTES: paramsEdit.Edxnote,
                        ENABLE_DASHBOARD_SEARCH: paramsEdit.Search,
                        INSTRUCTOR_BACKGROUND_TASKS: paramsEdit.Tasks,
                        ENABLE_COURSE_DISCOVERY: paramsEdit.Course
                    },
                    displayed_timezone: paramsEdit.TimeZone,
                    language_code: paramsEdit.Lenguage,
                    CREATION_DATE: params.data.CREATION_DATE,
                    user_email: paramsEdit.Email,
                    SUBSCRIPTION: params.data.SUBSCRIPTION
                }

        };

      let options = this.createHeader(RequestMethod.Patch);
      console.log(body);
      return this.http.patch( `${this.postsUrl}/customerdata/${params.id}/`,body,options).map( response => {
        if (response)
           return this.userJson = {"status":"200","data":{response}};
      }).catch((error: any) => Observable.throw(this.userJson=error.json()));
  }

  /**
   * headers of the request
   */
  public createHeader(requestMethod:number){
      let _headers: Headers = new Headers();
      let options = new RequestOptions({ method: requestMethod, headers: _headers });
     _headers.append('Content-Type', 'application/json');
     return options;
  }



}
