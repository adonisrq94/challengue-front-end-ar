import { Component, OnInit,Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import {User} from '../../classes/user';
import { FormEditUser } from '../../form/form-edit-user';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']

})
export class UserComponent implements OnInit {

  /**
   * boolean to see the form of user
   */
  edit:boolean = false;
  /**
   * boolean to see the view of information
   */
  show:boolean = true;

  loader:boolean = true;
  errorMessage:boolean = false;
  successMessage:boolean = false;
  message:string = '';
  user:any = {};
  /**
   * form for edit users
   */
  form : FormGroup;
  userFild:Object =  {};


  /**
   * json of the timezones
   */
  timeZones = [
        {
          id:"America/NewYork",
          zona:"America/NewYork"},
        {
          id:"America/Bogota",
          zona:"America/Bogota"},
  ];

  /**
   * json of theme for users
   */
  Themes = [
        {
        id:"Simply Fabulous",
        theme:"Simplemente fabuloso"},
        {
        id:"Tropical Island",
          theme:"Isla tropical"},
        {
        id:"Safari",
        theme:"Safari"},
        {
        id:"Tranquility",
        theme:"Tranquilidad"},
        {
        id:"Tropical Island",
        theme:"Isla tropical"},
        {
        id:"Mustache Bash",
        theme:"Bigote Bash"},
        {
        id:"Candy Crush",
        theme:"Candy Crush"},
        {
        id:"Garden Party",
        theme:"Fiesta de jardin"}
  ];

  /**
   * lenguage option for user
   */
  lenguages = [
        {
          id:"zh",
          lenguage:"Chino"},
        {
          id:"it",
          lenguage:"Italiano"},
        {
          id:"en",
          lenguage:"Ingles"},
        {
          id:"es",
          lenguage:"Español"},
        {
          id:"fr",
          lenguage:"Frances"},
        {
          id:"de",
          lenguage:"Aleman"}
  ];



  constructor(private activatedRoute: ActivatedRoute,
                       public userSevice:UserService){

      this.activatedRoute.params.subscribe(params =>{
          this.getUser(params.id);
          let formEdit:FormEditUser = new FormEditUser(this.user);
          //get structure of the form
          this.form = formEdit.GetFormBuilder();
      })
  } 

  ngOnInit() {
    this.loader = false;
  }

  /**
   *  get the informacion about specific user
   */
   getUser(id:string) {
    this.userSevice.getUser(id).subscribe(data =>{
         this.user = data;
         this.userFild={
                   Theme:this.user.data.theme_name,
                   Certificates:this.user.data.ENABLED_FEATURES.CERTIFICATES_INSTRUCTOR_GENERATION,
                   Tasks:this.user.data.ENABLED_FEATURES.INSTRUCTOR_BACKGROUND_TASKS,
                   Courseware:this.user.data.ENABLED_FEATURES.ENABLE_COURSEWARE_SEARCH,
                   Course:this.user.data.ENABLED_FEATURES.ENABLE_COURSE_DISCOVERY,
                   Search:this.user.data.ENABLED_FEATURES.ENABLE_DASHBOARD_SEARCH,
                   Edxnote:this.user.data.ENABLED_FEATURES.ENABLE_EDXNOTES,
                   Lenguage:this.user.data.language_code,
                   TimeZone:this.user.data.displayed_timezone,
                   Email:this.user.data.user_email,
                 };
    },
     error=>{});
     return this.userFild;
  }


  /**
   * update information of the users
   */
  updateUser() {
    this.errorMessage = false;
    let limit:number=1;
    let params:any =this.form.value;
    let features:number=this.featuresCount();
    if ((features<=1 && this.user.data.SUBSCRIPTION == "free" ||
        (features<=3 && this.user.data.SUBSCRIPTION == "basic"))
        || this.user.data.SUBSCRIPTION == "premium" ){
        this.userSevice.updateUser(params,this.user).subscribe(data =>{
             if (data.status == "200"){
                  this.user = data.data;
                  this.message = "La operacion se a realizado exitosamente!";
                  this.successMessage=true;
                  location.reload();

           }
        },
         error=>{});
   }else{
      if (params.SUBSCRIPTION == "basic")
          limit=3;
      this.message = "Usted solo puede habilitar el siguiente numero de caracteristicas :"+limit;
      this.errorMessage = true;
   }}

   /**
    * count the features of the user
    */
   featuresCount():number{
     let features:number=0;
     let params:any =this.form.value;
     for (let key in params) {
       let value = params[key];
       if (value == true)
       features++;
     }
     return features;
   }

   /**
    * change the display of show and form views
    */
  changeForm(){
    this.show = !this.show;
    this.edit = !this.edit;
    this.form.setValue(this.userFild);

  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}
