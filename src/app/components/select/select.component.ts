import { Component, OnInit } from '@angular/core';
import { User} from '../../classes/user';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  /**
   * json of information about user
   */
  public userJson:any;
  public user:User;

  constructor(public  userSevice:UserService) {

      this.user = new User();

  }

  ngOnInit() {
     this.userSevice.getUsers().subscribe(data =>{

     },
      error=>{this.userJson = {"status":"500","error":error}});
  }

}
