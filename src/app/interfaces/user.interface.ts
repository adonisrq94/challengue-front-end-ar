import {Features} from './features.interface';
export interface User {

    id?:string,
    banner_message:string,
    LAST_PAYMENT_DATE:string,
    theme_name:string,
    user_profile_image:string,
    ENABLED_FEATURES:Features,
    displayed_timezone: string,
    language_code: string,
    CREATION_DATE: string,
    user_email: string,
    SUBSCRIPTION: string,

}
